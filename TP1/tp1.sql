DROP TABLE CLIENTS cascade constraints;
DROP TABLE COMMANDE cascade constraints;
DROP TABLE ARTICLES cascade constraints;
DROP TABLE POINT_DE_DISTRIBUTION cascade constraints;
DROP TABLE COLIS cascade constraints;
DROP TABLE FAIRE_PARTIE cascade constraints;
DROP TABLE COMPOSER cascade constraints;


CREATE TABLE CLIENTS (
    referenceCl NUMBER(38) PRIMARY KEY NOT NULL,
    nom varchar(100),
    prenom varchar(100),
    adresse varchar(100),
    email varchar(100)
);


CREATE TABLE COMMANDE (
    referenceCom NUMBER(38) NOT NULL,
    dateCom DATE,
    montantTotalCom NUMBER(38),
    referenceCl NUMBER(38) NOT NULL,
    PRIMARY KEY (referenceCom, referenceCl),
    FOREIGN KEY (referenceCl) REFERENCES CLIENTS (referenceCl) 
);


CREATE TABLE ARTICLES (
    num_ref number(38) NOT NULL,
    designation VARCHAR(100),
    prix number(38),
    tva number(38),
    PRIMARY KEY (num_ref)
);

CREATE TABLE POINT_DE_DISTRIBUTION (
    referencePointDeDistrib NUMBER(38),
    adressePointDeDistric VARCHAR(100),
    PRIMARY KEY (referencePointDeDistrib)
);

CREATE TABLE COLIS (
    numeroColis NUMBER(38),
    indicateurRetratColis NUMBER(1),
    -- 0 si pas retiré
    -- 1 si retiré
    referenceCom NUMBER(38),
    referenceCl NUMBER(38) NOT NULL,
    PRIMARY KEY (numeroColis, referenceCl, referenceCom),
    FOREIGN KEY (referenceCom, referenceCl) REFERENCES COMMANDE (referenceCom, referenceCl)
);

CREATE TABLE FAIRE_PARTIE (
    referenceCom NUMBER(38),
    referenceCl NUMBER(38) NOT NULL,
    num_ref number(38) NOT NULL,
    quantiteeCommandee NUMBER(38),
    PRIMARY KEY (referenceCom, referenceCl, num_ref),
    FOREIGN KEY (referenceCom, referenceCl) REFERENCES COMMANDE (referenceCom, referenceCl),
    FOREIGN KEY (num_ref) REFERENCES ARTICLES (num_ref)
);

CREATE TABLE COMPOSER (
    num_ref number(38) NOT NULL,
    numeroColis NUMBER(38),
    referenceCom NUMBER(38),
    referenceCl NUMBER(38) NOT NULL,
    quantiteeExpediee NUMBER(38),
    quantiteeAcceptee NUMBER(38),
    PRIMARY KEY (num_ref, numeroColis, referenceCom, referenceCl),
    FOREIGN KEY (num_ref) REFERENCES ARTICLES (num_ref),
    FOREIGN KEY (numeroColis, referenceCom, referenceCl) REFERENCES COLIS (numeroColis, referenceCom, referenceCl)
);